var assetsFolder = './';

var Encore = require('@symfony/webpack-encore');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

Encore
    .setOutputPath('./less/compiled/')
    .setPublicPath('/wp-content/themes/mangu/less/compiled/')
    .autoProvidejQuery()
    .enableLessLoader()
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .configureUglifyJsPlugin(function(){
        console.log(this);
    });
;

Encore.addEntry('less', assetsFolder + 'less/req.js');

var config = Encore.getWebpackConfig();

// Remove the old version first
config.plugins = config.plugins.filter(
    plugin => !(plugin instanceof webpack.optimize.UglifyJsPlugin)
);

// Add the new one
config.plugins.push(new UglifyJsPlugin({
    uglifyOptions: {
        mangle: Encore.isProduction(),
        compress: Encore.isProduction()
    },
    sourceMap: !Encore.isProduction()
}));


module.exports = config;