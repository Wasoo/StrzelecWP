<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Mangu
 * @since Mangu 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<button class="hamburger hamburger--spring" type="button">
		<span class="hamburger-box">
		<span class="hamburger-inner"></span>
		</span>
</button>

<div class="navbar">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

    <header id="mainh">
				<nav>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
				</nav>

        	<a href="<?=home_url()?>" id="logo"/></a>

				<div class="adres">
					<span>Jejkowice 44-290, ul. Główna 11</span>
					<span>503 658 730</span>
				</div>

    </header>
    <div id="main_slider_wrap">
        <div id="main_slider">
            <?php
                echo do_shortcode('[metaslider id=23]');
            ?>
        </div>
    </div>
    <div id="pasek">
        <?php
            $page = get_page(69);
            echo $page->post_content;
        ?>
    </div>
	<div id="main" class="site-main container">
