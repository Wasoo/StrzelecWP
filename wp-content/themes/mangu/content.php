<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Mangu
 * @since Mangu 1.0
 */
?>
	<div class="col-sm-4 column">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


			<header class="entry-header">
				<?php

					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		        echo '<div class="imgwrap">'.get_the_post_thumbnail(null,'gallery-element').'</div>';
					endif;
				?>

				<div class="entry-meta">

					<?php

						//edit_post_link( __( 'Edit', 'mangu' ), '<span class="edit-link">', '</span>' );
					?>
				</div><!-- .entry-meta -->
			</header><!-- .entry-header -->

			<?php if ( !is_single() ) : ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
			<div class="column_button">
				<?='<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">Więcej...</a>';?>
			</div>
			<?php else : ?>
			<div class="entry-content">
				<?php
					the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'mangu' ) );
					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'mangu' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
					) );
				?>
			</div><!-- .entry-content -->
			<?php endif; ?>

			<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
		</article><!-- #post-## -->
	</div>
