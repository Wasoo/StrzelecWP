<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Mangu
 * @since Mangu 1.0
 */
?>

		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">
                        44-290 Jejkowice, ul. Główna 11  • 604 891 970 • Czynne codziennie od 9:00 do 21:00<br/>
                    Wszystkie prawa zastrzeżone  © 2014  karczmastrzelec.pl<br/>
                    <div class="wzorek"></div>

		</footer><!-- #colophon -->
                <div class="clear"></div>
	</div><!-- #page -->

	<?php wp_footer(); ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function() {

			  var $hamburger = $(".hamburger");
			  $hamburger.on("click", function(e) {
			    $hamburger.toggleClass("is-active");
					$('body').toggleClass('off-canvas-is-active');
			  });
		});
	</script>

</body>
</html>
